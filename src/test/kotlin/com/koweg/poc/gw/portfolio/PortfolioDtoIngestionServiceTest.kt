package com.koweg.poc.gw.portfolio

import com.koweg.poc.gw.GatewayService
import com.koweg.poc.gw.config.MessagingIntegrationProperties
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import io.mockk.junit5.MockKExtension
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.amqp.rabbit.core.RabbitTemplate
import java.nio.file.Paths

@ExtendWith(MockKExtension::class)
class PortfolioDtoIngestionServiceTest {

    @InjectMockKs()
    lateinit var portfolioDataIngestionService: PortfolioDataIngestionService



    @Test
    fun `should successfully ingest and transform csv data`(){

        val path = Paths.get("src", "test", "resources", "test_data_watchlist.csv")
        val  data = portfolioDataIngestionService.ingestData(path.toFile().inputStream())
        assertThat(data).isNotNull
        assertThat(data?.positionData?.size).isEqualTo(18)

    }

}