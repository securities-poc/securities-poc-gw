package com.koweg.poc.gw.portfolio

import com.koweg.poc.gw.GatewayService
import com.koweg.poc.gw.portfolio.data.PortfolioDto
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

@ExtendWith(SpringExtension::class)
@WebMvcTest(PortfolioDataUploadResource::class)
class PortfolioDtoUploadResourceTest {

    @Autowired
    lateinit var mockMvc: MockMvc

    @MockkBean
    private lateinit var gatewayService: GatewayService

    @Test
    fun `should load file stream for ingestion`(){
        every { gatewayService.portfolioUpdated(any()) } returns (Unit)

        val mockFile = MockMultipartFile("csv",
                "watchlist.csv",
                MediaType.APPLICATION_OCTET_STREAM_VALUE,
                "MSFT;300;147.33".byteInputStream())
        mockMvc.perform(multipart("/api/portfolios")
                .file(mockFile))
                .andExpect(status().isAccepted)

        verify { gatewayService.portfolioUpdated(any()) }
    }

}