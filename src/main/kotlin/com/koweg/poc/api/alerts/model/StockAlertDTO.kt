package com.koweg.poc.api.alerts.model

import org.springframework.boot.jackson.JsonComponent

@JsonComponent
data class StockAlertDTO(var id: String?,
                         var isin: String?,
                         var quantity: Int?,
                         var lowerLimit: Double?,
                         var upperLimit: Double?
) {
    constructor() : this(null, null, null, null, null)
}