package com.koweg.poc.api.portfolio.model

import org.springframework.boot.jackson.JsonComponent
import java.time.LocalDateTime

@JsonComponent
data class Portfolio(var portfolioName: String?,
                     var currency: Currency?,
                     var positions: List<Position>?,
                     var date: LocalDateTime?
){
    constructor(): this(null, null, null,null)
}
