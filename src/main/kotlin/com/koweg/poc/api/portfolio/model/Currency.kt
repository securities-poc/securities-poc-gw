package com.koweg.poc.api.portfolio.model

enum class Currency {
    CHF, CNY, EUR, GBP, USD
}