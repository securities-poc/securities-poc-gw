package com.koweg.poc.api.portfolio.model

import org.springframework.boot.jackson.JsonComponent

@JsonComponent
data class Position(
        var isin: String?,
        var symbol: String?,
        var stockName: String?,
        var marketPrice: Double?,
        var quantitiy: Int?,
        var currency: String?,
        var unitPrice: Double?,
        var bookCost: Double?
){
    constructor(): this(null,null,null,null,null,null,null,null)
}
