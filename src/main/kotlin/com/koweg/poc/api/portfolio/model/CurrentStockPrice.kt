package com.koweg.poc.api.portfolio.model

import org.springframework.boot.jackson.JsonComponent

@JsonComponent
data class CurrentStockPrice(var isin:  String?,
                             var price: Double?
) {
    constructor(): this(null,null)
}