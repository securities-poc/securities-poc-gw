package com.koweg.poc.api.forex.model

import org.springframework.boot.jackson.JsonComponent

@JsonComponent
data class QuotesDTO(var currency: String?,
                     var quote: Double?
){
    constructor(): this(null,null)
}