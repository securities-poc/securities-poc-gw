package com.koweg.poc.api.forex.model

import org.springframework.boot.jackson.JsonComponent

@JsonComponent
data class CurrencyQuoteDTO (
        var baseCurrency: String?,
        var timeStamp: Int?,
        var quotes: List<QuotesDTO>?
){
    constructor(): this(null,null, null)
}
