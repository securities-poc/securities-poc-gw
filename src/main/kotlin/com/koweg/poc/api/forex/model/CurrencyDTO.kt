package com.koweg.poc.api.forex.model

enum class CurrencyDTO {
    CHF, CNY, EUR, GBP, USD
}