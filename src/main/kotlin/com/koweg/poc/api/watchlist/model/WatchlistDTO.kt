package com.koweg.poc.api.watchlist.model

import org.springframework.boot.jackson.JsonComponent

@JsonComponent
data class WatchlistDTO(var isin: String?,
                        var symbol: String?,
                        var stockName: String?,
                        var marketRate: Double?,
                        var currency: String?
){
    constructor(): this(null,null,null,null,null)
}