package com.koweg.poc

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.boot.jackson.JsonComponent

@JsonComponent
data class CompanyInfo(
    @JsonProperty("address")
    val address: String?,
    @JsonProperty("city")
    val city: String?,
    @JsonProperty("country")
    val country: String?,
    @JsonProperty("currency")
    val currency: String?,
    @JsonProperty("cusip")
    val cusip: String?,
    @JsonProperty("description")
    val description: String?,
    @JsonProperty("exchange")
    val exchange: String?,
    @JsonProperty("ggroup")
    val ggroup: String?,
    @JsonProperty("gind")
    val gind: String?,
    @JsonProperty("gsector")
    val gsector: String?,
    @JsonProperty("gsubind")
    val gsubind: String?,
    @JsonProperty("ipo")
    val ipo: String?,
    @JsonProperty("isin")
    val isin: String?,
    @JsonProperty("naics")
    val naics: String?,
    @JsonProperty("name")
    val name: String?,
    @JsonProperty("phone")
    val phone: String?,
    @JsonProperty("state")
    val state: String?,
    @JsonProperty("ticker")
    val ticker: String?,
    @JsonProperty("weburl")
    val weburl: String?
)