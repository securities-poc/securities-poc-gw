package com.koweg.poc

import com.fasterxml.jackson.databind.ObjectMapper
import com.koweg.poc.gw.config.KafkaIntegrationProperties
import com.koweg.poc.gw.config.MessagingIntegrationProperties
import com.koweg.poc.gw.config.ThirdPartyApiCredentialProperties
import org.apache.kafka.clients.admin.NewTopic
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.amqp.core.Binding
import org.springframework.amqp.core.BindingBuilder
import org.springframework.amqp.core.Queue
import org.springframework.amqp.core.TopicExchange
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.config.TopicBuilder
import org.springframework.kafka.core.DefaultKafkaProducerFactory
import org.springframework.kafka.core.KafkaAdmin
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.core.ProducerFactory
import org.springframework.kafka.support.serializer.JsonSerializer
import org.springframework.web.reactive.function.client.WebClient

@SpringBootApplication
@EnableConfigurationProperties(ThirdPartyApiCredentialProperties::class,
		MessagingIntegrationProperties::class,
		KafkaIntegrationProperties::class)
@EnableEurekaClient
class SecuritiesPocGwApplication

fun main(args: Array<String>) {
	runApplication<SecuritiesPocGwApplication>(*args)
}


@Configuration
class WebClientConfiguration {
	@Bean
	fun externalApiClient(): WebClient {
		return WebClient.create()
				.mutate()
				.build()
	}
	@Bean
	fun subscriptionInitialiser(config: ThirdPartyApiCredentialProperties, @Autowired externalApiClient: WebClient) =  ApplicationRunner {
	}
}

@Configuration
class MessagingConfiguration{
	@Bean
	fun exchangeToQueueBinding(config: MessagingIntegrationProperties): Binding {
		return  BindingBuilder.bind(Queue(config.portfolio.queue))
				.to(TopicExchange(config.portfolio.exchange))
				.with(config.portfolio.routingkey)
	}
	@Bean
	fun messagingTemplate(connectionFactory: ConnectionFactory, objectMapper: ObjectMapper): RabbitTemplate {
		val rabbitTemplate = RabbitTemplate(connectionFactory)
		rabbitTemplate.setMessageConverter(Jackson2JsonMessageConverter(objectMapper))
		return  rabbitTemplate
	}
}

@Configuration
class KafkaConfiguration{

	@Bean
	fun portfolioKafkaTemplate(props: KafkaIntegrationProperties): KafkaTemplate<String, Any> {
		return KafkaTemplate<String, Any>(portfolioProducerConfig(props))
	}

	@Bean
	fun portfolioProducerConfig(props: KafkaIntegrationProperties): ProducerFactory<String, Any> {
		var config: MutableMap<String, Any> = mutableMapOf()
		config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, props.bootstrapServers)
		config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer::class)
//		config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer::class)
		config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer::class)
		return DefaultKafkaProducerFactory(config)
	}

}

@Configuration
class KafkaTopicsConfiguration{

	@Bean
	fun kafkaAdmin(props: KafkaIntegrationProperties): KafkaAdmin {
		var config: MutableMap<String, Any> = mutableMapOf()
		config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, props.bootstrapServers)
		return  KafkaAdmin(config)
	}

	@Bean
	fun portfolioTopic(props: KafkaIntegrationProperties) : NewTopic{
		return TopicBuilder
				.name(props.portfolio.producerTopic)
				.compact()
				.partitions(1)
				.replicas(1)
				.build()
	}

}