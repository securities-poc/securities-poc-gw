package com.koweg.poc.gw

import com.koweg.poc.gw.portfolio.PortfolioDataIngestionService
import com.koweg.poc.gw.portfolio.PortfolioNotificationService
import com.koweg.poc.gw.portfolio.data.PortfolioDto
import org.springframework.stereotype.Service
import java.util.*
import java.io.InputStream

@Service
class GatewayService (val portfolioNotificationService: PortfolioNotificationService,
                      val portfolioDataIngestionService: PortfolioDataIngestionService){

    private fun publishPortfolioUpdate(portfolioData: PortfolioDto?): Unit {
        portfolioNotificationService.publishPortfolioData(portfolioData)
    }

    private fun extractPortfolioData(data: InputStream): PortfolioDto? {
        return  portfolioDataIngestionService.ingestData(data)
    }

    fun portfolioUpdated(data: InputStream): Unit {
        publishPortfolioUpdate(extractPortfolioData(data))
    }
}