package com.koweg.poc.gw.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("messaging-int-rabbitmq")
class MessagingIntegrationProperties{
	val portfolio = Portfolio()
	val watchlist = Watchlist()
	val alert = Alert()
	val subscription = Subscription()

	class Portfolio{
		lateinit var exchange: String
		lateinit var routingkey: String
		lateinit var queue: String
	}

	class Watchlist{
		lateinit var exchange: String
		lateinit var routingkey: String
		lateinit var queue: String
	}

	class Alert{
		lateinit var exchange: String
		lateinit var routingkey: String
		lateinit var queue: String
	}

	class Subscription{
		lateinit var stockDataQueue: String
		lateinit var fxDataQueue: String
	}

}