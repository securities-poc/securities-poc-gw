package com.koweg.poc.gw.config

import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.boot.context.properties.ConfigurationProperties

@ConstructorBinding
@ConfigurationProperties("kafka-int")
class KafkaIntegrationProperties{

    lateinit var bootstrapServers : String
    val portfolio = Portfolio()

    class Portfolio{
        lateinit var producerTopic: String
        lateinit var numPartitions: String
        lateinit var replicationFactor: String
    }

}