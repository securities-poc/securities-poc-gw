package com.koweg.poc.gw.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("thirdparty-api")
class ThirdPartyApiCredentialProperties{
	val finnhubApi = FinnHubApi()
	val alpacaApi = AlpacaApi()
	class FinnHubApi {
		lateinit var endPoint: String
		lateinit var apiKey: String
	}
	class AlpacaApi {
		lateinit var endPoint: String
		lateinit var apiKeyId: String
		lateinit var secretKey: String
	}
}