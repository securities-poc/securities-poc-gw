package com.koweg.poc.gw.portfolio.data

import com.opencsv.bean.CsvBindByPosition
import org.apache.commons.lang3.builder.ReflectionToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle

data class PositionDto(
        @CsvBindByPosition(position = 1)
        var isin: String?,
        @CsvBindByPosition(position = 2)
        var stockName: String?,
        @CsvBindByPosition(position = 3)
        var quantity: String?,
        @CsvBindByPosition(position = 5)
        var unitPrice: String?,
        @CsvBindByPosition(position = 6)
        var currency: String?,
        @CsvBindByPosition(position = 7)
        var bookCost: String?,
        @CsvBindByPosition(position = 10)
        var marketPrice: String?
) {
    constructor() : this(null, null, null, null, null, null, null)
}