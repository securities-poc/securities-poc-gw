package com.koweg.poc.gw.portfolio

import com.koweg.poc.gw.config.KafkaIntegrationProperties
import com.koweg.poc.gw.portfolio.data.PortfolioDto
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Service
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

@Service
class PortfolioNotificationService(val portfolioKafkaTemplate: KafkaTemplate<String, Any>,
                                   val config: KafkaIntegrationProperties) {

    val mapper = jacksonObjectMapper()

    fun publishPortfolioData(portfolioData: PortfolioDto?): Unit {
        println("----------- Sending -------------------")
        for((k,v) in portfolioKafkaTemplate.metrics()){
            println("${k} ---> ${v}")
        }
        portfolioData!!.positionData!!.forEach { p -> println(p.toString()) }
        portfolioKafkaTemplate.send(config.portfolio.producerTopic, mapper.writeValueAsString(portfolioData))
        //portfolioKafkaTemplate.send(config.portfolio.producerTopic, portfolioData)
    }
}