package com.koweg.poc.gw.portfolio.data

import java.time.LocalDateTime

data class PortfolioDto(
        var date: LocalDateTime?,
        var portfolioName: String?,
        var currency: String?,
        var positionData: List<PositionDto>?){
    constructor() : this(null, null, null, null)
}