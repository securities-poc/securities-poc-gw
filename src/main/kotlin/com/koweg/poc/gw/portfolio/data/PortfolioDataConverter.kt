package com.koweg.poc.gw.portfolio.data

import com.koweg.poc.api.portfolio.model.Portfolio
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper
interface PortfolioDataConverter {
    @Mapping(source = "positionData", target = "positions")
    fun convertToModel(portfolioDto: PortfolioDto): Portfolio
    @InheritInverseConfiguration
    fun convertToDto(portfolio: Portfolio): PortfolioDto
}