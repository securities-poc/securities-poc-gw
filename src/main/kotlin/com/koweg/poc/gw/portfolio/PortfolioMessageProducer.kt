package com.koweg.poc.gw.portfolio

import com.koweg.poc.gw.config.KafkaIntegrationProperties
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.kafka.core.DefaultKafkaProducerFactory
import org.springframework.kafka.core.KafkaTemplate
import java.util.*

class PortfolioMessageProducer (config: KafkaIntegrationProperties){

    private val kafkaTemplate : KafkaTemplate<String, Any>

    init {
        val props : MutableMap<String, Any> = mutableMapOf()
        props[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = config.bootstrapServers
        props[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
        props[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
        kafkaTemplate = KafkaTemplate(DefaultKafkaProducerFactory(props))
    }
}