package com.koweg.poc.gw.portfolio

import com.koweg.poc.gw.GatewayService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("/api")
class PortfolioDataUploadResource (val gatewayService: GatewayService){

    @PostMapping("/portfolios", consumes = arrayOf(MediaType.MULTIPART_FORM_DATA_VALUE))
    fun watchlistUpload(@RequestParam("csv") data: MultipartFile): ResponseEntity<Void> {
            gatewayService.portfolioUpdated(data.inputStream)
        return ResponseEntity.accepted().build<Void>()
    }

}
