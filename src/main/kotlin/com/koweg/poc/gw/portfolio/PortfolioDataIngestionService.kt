package com.koweg.poc.gw.portfolio

import com.koweg.poc.gw.portfolio.data.PortfolioDto
import com.koweg.poc.gw.portfolio.data.PositionDto
import com.opencsv.bean.*
import org.springframework.stereotype.Service
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.nio.charset.StandardCharsets
import java.time.LocalDateTime
import java.util.*

interface DataIngestionService <D>{
    fun ingestData(data: InputStream): D
}


@Service
class PortfolioDataIngestionService : DataIngestionService <PortfolioDto?> {

    override fun ingestData(data: InputStream) : PortfolioDto?{
        val reader = BufferedReader(InputStreamReader(data, StandardCharsets.UTF_8.name()))
        val strategy = ColumnPositionMappingStrategy<PositionDto>()
        strategy.setType(PositionDto::class.java)
        val positions = CsvToBeanBuilder<PositionDto>(reader)
                .withMappingStrategy(strategy)
                .withFilter(PortfolioDataFilter())
                .withSeparator(';')
                .withQuoteChar('"')
                .withIgnoreEmptyLine(true)
                .withIgnoreLeadingWhiteSpace(true)
                .build()
                .parse()
        return PortfolioDto(LocalDateTime.now(), "My Portfolio", "EUR", positions)

    }

    class PortfolioDataFilter: CsvToBeanFilter{
        override fun allowLine(line: Array<out String>?): Boolean {
            val dateLine = "Depotbewertung vom "
            val headerLine = "WP-Art"
            val portfolioNameLine = "Watchlist"
            val validLine = "Aktien"
            val token = line?.get(0) ?: ""
            //val numColumns = line?.size ?: 0
            return when {
                token.startsWith(dateLine, true) -> {
                    false
                }
                token.startsWith(portfolioNameLine, true) -> {
                    false
                }
                token.startsWith(headerLine, true) -> {
                    false
                }
                token.startsWith(validLine) -> true
                token.isNullOrEmpty() || token.isNullOrBlank() -> false
                else -> false
            }
        }
    }

}



