# Securities Gateway

This service implements the [anti-corruption layer pattern](https://docs.microsoft.com/en-us/azure/architecture/patterns/anti-corruption-layer)
The main goals and features of the service are:

- Normalisation of communication semantics of external systems/dependencies by:
  - establishing a common message and transport protocol for external data consumption.
  - providing a standard internal interface definition for all transformed external data.
- Enabling consumption of external data by both command-based mechanisms and event-driven triggers.




### Deployment

##### Building and running locally
This build depends on a local instance of Eureka listening on port 7000.
It also requires a running instance of Rabbit MQ that has all necessary exchanges, bindings and queues already configured.  

```bash
 docker run --name securities-rabbitmq -e RABBITMQ_DEFAULT_USER=guest -e RABBITMQ_DEFAULT_PASS=guest -p 15672:15672 -p 5672:5672 rabbitmq:3.7.17-management-alpine
```
```bash
 mvn clean package -Dspring.profiles.active=local
```
```bash
 java -jar target/securities-poc-gw.jar --spring.profiles.active=local
```


##### Testing local deployment for CSV data ingestion via HTTP upload
```bash
 curl -F 'csv=@/home/larinde/data/watchlist.csv' http://localhost:7100/api/portfolios -v
```

##### Integration environment build
```bash
 mvn clean compile jib:dockerBuild  -Dspring.profiles.active=int -DskipTests
```


### TODO
- Data model for feed subscriptions
- Data store for temp storage of large data feeds
- Async API spec for internal consumers
- Abstraction of MoM 
